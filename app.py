#!/usr/bin/env python
from flask import Flask, render_template, jsonify, request
import pathlib
from datetime import datetime

import pickle

DUMP_FILE = pathlib.Path().resolve() / "telegram.pickle"

app = Flask(__name__)


def read_messages():
    messages = []
    if DUMP_FILE.exists():
        with open(DUMP_FILE, "rb") as f:
            messages = pickle.load(f)
    return messages


@app.route("/")
def index():
    return render_template("index.html")


def get_messages():
    now = request.args.get("time", datetime.now())
    messages = read_messages()
    sleep_seconds = 86400
    for message in messages:
        if message['date'] >= (int(now.timestamp()) - sleep_seconds):
            yield message


@app.route("/messages")
def messages():
    #for message in get_messages():
        #name = message['chat']['first_name']
        #print(name)
        #out += str(message)
    msgs = list(get_messages())
    return jsonify(msgs)


if __name__ == '__main__':
    app.run(debug=True)
