import pathlib
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

from settings import TOKEN

import pickle


DUMP_FILE = pathlib.Path().resolve() / "telegram.pickle"


def dump_messages(message):
    if DUMP_FILE.exists():
        with open(DUMP_FILE, "rb") as f:
            messages = pickle.load(f)
    else:
        messages = []
    messages.append(message.to_dict())
    with open(DUMP_FILE, "wb") as f:
        pickle.dump(messages, f)


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    #update.message.reply_text(update.message.text)
    print(f"Received: {update.message.text}")
    dump_messages(update.message)


def main():
    print("Starting Telegram bot")
    updater = Updater(TOKEN)

    dispatcher = updater.dispatcher

    #dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(MessageHandler(Filters.all, echo))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
